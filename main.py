import cgi
import os

from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
import urllib

from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import images
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import mail as mail

#import jinja2

#user = users.get_current_user()
def render_template(handler, templatename, templatevalues={}):
	path = os.path.join(os.path.dirname(__file__), 'templates/' + templatename)
	html = template.render(path, templatevalues)
	handler.response.out.write(html);
	
class logInGoogle(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			#self.response.out.write("Welcome, " + user.nickname() + '<br>')
			logout_url = users.create_logout_url('/')
			usernickname = user.nickname()
			page_params = {
				'lo_url':logout_url,
				'username': usernickname
			}
			#self.response.out.write('<a href="' + logout_url + '">click here to log out</a>')
			render_template(self, 'index.html', page_params)
		else:
			login_url = users.create_login_url()
			render_template(self, 'homepage.html')
			self.response.out.write('Please log in: ')
			self.response.out.write('<a href="' + login_url + '">click here</a>')

class MyMusicPage(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user();

		q = MyMusic.query(MyMusic.user == user.email())
		result = list()
		for i in q.fetch():
			result.append(i)

		if os.environ.get('HTTP_HOST'):
			url = os.environ['HTTP_HOST']
		else:
			url = os.environ['SERVER_NAME']

		pageparams = {
			'url': url,
			'upload_url': blobstore.create_upload_url('/upload'),
			'lo_url' : users.create_logout_url('/'),
			'username' : user.nickname(),
			'tracks' : result,
			'current': 'active'
		}
		render_template(self, 'myuploads.html', pageparams)

class trackHandler(blobstore_handlers.BlobstoreDownloadHandler):
	def get(self, rec):
		user = users.get_current_user()
		rec = str(urllib.unquote(rec))
		self.response.out.write(rec)
		blob_info = blobstore.BlobInfo.get(rec)
		#music_id = self.request.get('key')

		#if not blobstore.get(music_id):
			#self.error(404)
		#else:
		self.send_blob(blob_info)
		#params = {
		#	'name':  music_id
		#}
		#render_template(self, 'music.html', params)

class MusicStore(blobstore_handlers.BlobstoreUploadHandler):
	def post(self):
		user = users.get_current_user()
		try:
			upload_files = self.get_uploads()
			#############################################
			blob_info = upload_files[0]
			name = self.request.get('name')
			user_music = MyMusic()
			user_music.name = name
			user_music.track = blob_info.key()
			user_music.user = user.email()
			#user_music.blobkey = blob_info.key()
			user_music.put()

			music_id = user_music.key.urlsafe()
			self.redirect('/myuploads')
			#self.response.out.write('UPLOADED')
		except:
			self.error(500)

class contactUs(webapp2.RequestHandler):
	def get(self):
		name = self.request.get('name')
		message = self.request.get('message')
		email = self.request.get('email')
		params = {
			'name': name,
      		'message': message,
			'email': email,
		}
		render_template(self, 'contactus.html', params)
	def post(self):
	    #name = self.request.get('name')
        #message = self.request.get('message')
        #email = self.request.get('email')
        #params = {
        #	'name': name,
		#	'message': message,
		#	'email': email,
	    #}	
	    from_address = 'contact@panamb1078.appspotmail.com'
	    subject = 'Contact from ' + name
	    body = 'Message from ' + email + ':\n\n' + message
	    mail.send_mail(from_address, 'panamb1028@gmail.com', subject, body)
	    self.redirect("/contactus")
	    #render_template(self, 'contactus.html', params)

class profilePageHandler(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()

		q = MyImage.query(MyImage.user == user.email())
		q1 = q.filter(MyImage.name == 'profile')

		i = list()
		i.append(q1.fetch())

		image_id = 'nothinghere'

		if not i:
			image_id = i.pop(0).key.urlsafe()
			my_image = ndb.Key(urlsafe=image_id).get()

		params = {
			'username' : user.nickname(),
			'lo_url' : users.create_logout_url('/'),
			'current': 'active',
			'image_id' : image_id,
			'upload_url': blobstore.create_upload_url('/uploadimage')
		}
		render_template(self, 'profile.html', params) 

class imageHandler(blobstore_handlers.BlobstoreUploadHandler):
	def post(self):
		user = users.get_current_user()
		try:
			upload_files = self.get_uploads()
			blob_info = upload_files[0]
			profile_image = MyImage()
			profile_image.image = blob_info.key()
			profile_image.user = user.email()
			profile_image.name = 'profile'
			profile_image.put()
			profile_id = profile_image.key.urlsafe()
			self.redirect('/profile')
		except:
			self.error(500)

class MyMusic(ndb.Model):
	name = ndb.StringProperty()
	track = ndb.BlobKeyProperty()
	user = ndb.StringProperty()
	#blobkey = blobstore.BlobReferenceProperty()
	
class MyImage(ndb.Model):
	name = ndb.StringProperty()
	image = ndb.BlobKeyProperty()
	user = ndb.StringProperty()

url_list = [ 
	('/', logInGoogle),
	('/myuploads', MyMusicPage),
	('/upload', MusicStore),
	('/music/([^/]+)?', trackHandler),
	('/contactus', contactUs),
	('/profile', profilePageHandler),
	('/uploadimage', imageHandler)
]

app = webapp2.WSGIApplication(url_list , debug = True)
			
			